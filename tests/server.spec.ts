import createServer from '../src/server';
import request from 'supertest';

describe ('Server Spec', () => {

  let server;

  beforeEach(() => {
    server = createServer ();
  });

  it ('should return 200', async () => {
    return request(server)
      .post('/checkout')
      .send([])
      .expect (200);
  });

  it ('should return totalAmout = 0 and loyaltyPoints = 0', async () => {
    return request(server)
      .post('/checkout')
      .send([])
      .expect (200, {
        totalAmount: 0,
        loyaltyPoints: 0
      });
  });

  it ('should return totalAmout = 0 and loyaltyPoints = 0', async () => {
    return request(server)
      .post('/checkout')
      .send(['01'])
      .expect (200, {
        totalAmount: 90,
        loyaltyPoints: 100
      });
  });

});
