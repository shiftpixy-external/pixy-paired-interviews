import { ProductsRepository } from '../../src/repository';

describe ('Repository Spec', () => {
  let repository;

  beforeEach (() => {
    repository = new ProductsRepository();
  });

  it ('should return an empty array', async () => {
    return expect (
      repository.getProductsByCode ([])
    ).resolves.toEqual([]);
  });

  it ('should return an array of length 2', async () => {
    return expect (
      repository.getProductsByCode (['01', '11'])
    ).resolves.toHaveLength(2);
  });

  it ('should return an array of length 2', async () => {
    return expect (
      repository.getProductsByCode (['01'])
    ).resolves.toEqual([
      {name: 'Amazing Watch', code: '01',group:'AMAZ_10_Watch',price: 100.00}
    ]);
  });

});
