import createServer from './server';
const port = 12345;

createServer().listen(port, () => console.log (`server listening on ${port}`));
