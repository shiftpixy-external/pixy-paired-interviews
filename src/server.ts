import { ProductsRepository, Product } from './repository';
import express, { Express } from 'express';

interface CheckoutResponse {
  totalAmount: number;
  loyaltyPoints: number;
}

export default ():Express => {
  const app = express ();
  app.use(express.json());

  const repository = new ProductsRepository ();

  app.post ('/checkout', async (req, res) => {
    const codes = req.body;
    const products = await repository.getProductsByCode (codes);

    const output:CheckoutResponse = { totalAmount: 0, loyaltyPoints: 0 };

    for (const p of products) {
      if (p.group.indexOf('_5_') > -1) {
        const discount = p.price * 0.05;
        output.totalAmount += p.price - discount;
      }
      else if (p.group.indexOf('_10_') > -1) {
        const discount = p.price * 0.1;
        output.totalAmount += p.price - discount;
      }
      else if (p.group.indexOf('_15_') > -1) {
        const discount = p.price * 0.15;
        output.totalAmount += p.price - discount;
      }
      else if (p.group.indexOf('_20_') > -1) {
        const discount = p.price * 0.2;
        output.totalAmount += p.price - discount;
      }

      if (p.group.startsWith('AMAZ')) {
        output.loyaltyPoints += 100;
      }
      else if (p.group.startsWith('WOND')) {
        output.loyaltyPoints += 150;
      }
    }

    res.json(output);
  });

  return app;
}


