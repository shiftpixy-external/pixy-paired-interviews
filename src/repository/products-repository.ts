export interface Product {
  name: string;
  code: string;
  group: string;
  price: number;
}

export class ProductsRepository {
  private readonly products:Product[];

  constructor () {
    this.products = [
      { name: 'Amazing Watch',    code: '01', group: 'AMAZ_10_Watch',  price: 100.00 },
      { name: 'Amazing Wallet',   code: '02', group: 'AMAZ_5_Wallet',  price: 30.00  },
      { name: 'Wonderful Watch',  code: '11', group: 'WOND_15_Watch',  price: 90.00  },
      { name: 'Wonderful Wallet', code: '12', group: 'WOND_20_Wallet', price: 34.00  },

      /* for next release */
      { name: 'Awesome Chair',    code: '31', group: 'AWES_25_Wallet', price: 40.00  },
      { name: 'Awesome Cellphone',code: '32', group: 'AWES_25_Phone',  price: 250.00 }
    ];
  }

  async getProductsByCode (codes:string[]): Promise<Product[]> {
    const output:Product[] = this.products.filter ((entry) => codes.includes(entry.code));
    return output;
  }

}
