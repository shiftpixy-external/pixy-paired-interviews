const { resolve } = require('path');

module.exports = {
  preset: 'ts-jest',
  rootDir: resolve(__dirname, '.'),
  roots: [
    '<rootDir>/src/',
    '<rootDir>/tests/',
  ],
  testEnvironment: 'node'
};
