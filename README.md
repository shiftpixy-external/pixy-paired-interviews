# Pair Programming Interview:

## Tools:

* Node.js v12
* Yarn 1.22

## Before starting the interview

Install all dependencies and run tests:

```
yarn install
yarn test
```

All tests should pass.

To run the application:

```
yarn build
yarn start
```

Or if you want to run in dev mode:
```
yarn dev
```

## Checkout System
The Product team asked the Dev team to delivery ASAP a PoC of a checkout-like system with the following requirements:
* it should receive as input an array of product codes
* it should compute the total amount of the purchase
* it should compute the total loyalty points the costumer earned with that purchase

Each product is stored with the following properties:
* `name`  (string): its name
* `code`  (string): its code
* `group` (string): its group
* `price` (number): its price

The product code has the following structure `XXXX_YY_S`:
* The `XXXX` part is 4-letter code for the category of the product. Currently there are two categories: `Amazing` (code `AMAZ`) and `Wonderful` (code `WOND`).
* The `YY` part is a 1 or 2-letter code for the discount applied over the product price.
* The `S` part is a string with no specific meaning.

For instance, if the product code is `AMAZ_10_PC` it means that it is a product of the category `Amazing` and it has `10%` off over its price. That is, if its price were $500.00 then the total amount would be $450.00.

Additionally, the Product team is deploying a novel loyalty program in which costumers earn points depending on the category of the products. It has the following rules:
* `Amazing` products earn 100 loyalty points each
* `Wonderful` products earn 150 loyalty points each

The checkout system deployed in the PoC calculates the total amount of the purchase and the total loyalty points earned by costumers for a given input array with product codes.

## Task
After the PoC has been demoed and validated by Operations team, your task is to extend the code by adding a new category of products: `Awesome` (code `AWES`).

When costumers buy an `Awesome` product, they should earn 200 loyalty points in each of them. Their price follow the same discount rules as other products.

In your task consider that the Operations team plans to create more categories in the future, likely before the initial launch of the system.

Feel free to change the structure of the code as you want for implementing the requirements. Just remember that this system was developed in a hurry for a fast PoC and now it's time to prepare it for production.
